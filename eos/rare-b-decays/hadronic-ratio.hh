/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2010, 2011, 2012, 2013, 2014 Danny van Dyk
 * Copyright (c) 2010 Christian Wacker
 * Copyright (c) 2014 Frederik Beaujean
 * Copyright (c) 2014 Christoph Bobeth
 * Copyright (c) 2015 Simon Braß
 *
 * This file is part of the EOS project. EOS is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * EOS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef EOS_GUARD_SRC_RARE_B_DECAYS_HADRONIC_RATIO_HH
#define EOS_GUARD_SRC_RARE_B_DECAYS_HADRONIC_RATIO_HH 1

#include <eos/rare-b-decays/decays.hh>
#include <eos/utils/complex.hh>
#include <eos/utils/options.hh>
#include <eos/utils/parameters.hh>
#include <eos/utils/private_implementation_pattern.hh>

namespace eos
{
    /*
     * Ratio: ee -> hadrons / ee -> mu mu
     */
    template <>
    class HadronicRatio<LowRecoil> :
        public ParameterUser,
        public PrivateImplementationPattern<HadronicRatio<LowRecoil>>
    {
        public:
            HadronicRatio(const Parameters & parameters, const Options & options);
            ~HadronicRatio();

	    double decay_momentum(const double & mass, const double & mass_p, const double & mass_k) const;
	    double orbital_sum(const double & mass, const double & mass_p, const double & mass_k) const;
	    double resonance_width(const double & s, const double & fit_width, const double & mass, const double & mass_p, const double & mass_k) const;
	    double total_width(const double & s, const int resonance) const;
	    double total_width(const double & mass) const;
	    std::complex<double> amplitude_finale_state(const double & s, const int & finale_state) const;

	    double ratio(const double & q) const;

	    double ratio_charm(const double & s) const;
	    
	    double ratio_narrow(const double & s) const;
	    double charm_loop_img(const double & s) const;
	    double charm_loop_real(const double & s) const;

	    complex<double> charm_loop(const double & s) const;
    };
}

#endif
